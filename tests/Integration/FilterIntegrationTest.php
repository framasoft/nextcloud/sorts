<?php

namespace OCA\Sorts\Tests\Integration\Controller;

use OCP\AppFramework\App;
use OCP\IRequest;
use PHPUnit\Framework\TestCase;


use OCA\Sorts\Db\Filter;
use OCA\Sorts\Db\FilterMapper;
use OCA\Sorts\Controller\FilterController;

class FilterIntegrationTest extends TestCase {
	private $controller;
	private $mapper;
	private $userId = 'john';

	public function setUp(): void {
		$app = new App('sorts');
		$container = $app->getContainer();

		// only replace the user id
		$container->registerService('userId', function () {
			return $this->userId;
		});

		// we do not care about the request but the controller needs it
		$container->registerService(IRequest::class, function () {
			return $this->createMock(IRequest::class);
		});

		$this->controller = $container->query(FilterController::class);
		$this->mapper = $container->query(FilterMapper::class);
	}

	public function testUpdate() {
		// create a new filter that should be updated
		$filter = new Filter();
		$filter->setTitle('old_title');
		$filter->setContent('old_content');
		$filter->setUserId($this->userId);

		$id = $this->mapper->insert($filter)->getId();

		// fromRow does not set the fields as updated
		$updatedFilter = Filter::fromRow([
			'id' => $id,
			'user_id' => $this->userId
		]);
		$updatedFilter->setContent('content');
		$updatedFilter->setTitle('title');

		$result = $this->controller->update($id, 'title', 'content');

		$this->assertEquals($updatedFilter, $result->getData());

		// clean up
		$this->mapper->delete($result->getData());
	}
}
