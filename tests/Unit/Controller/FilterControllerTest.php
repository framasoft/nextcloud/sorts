<?php

namespace OCA\Sorts\Tests\Unit\Controller;

use PHPUnit\Framework\TestCase;

use OCP\AppFramework\Http;
use OCP\IRequest;

use OCA\Sorts\Service\FilterNotFound;
use OCA\Sorts\Service\FilterService;
use OCA\Sorts\Controller\FilterController;

class FilterControllerTest extends TestCase {
	protected $controller;
	protected $service;
	protected $userId = 'john';
	protected $request;

	public function setUp(): void {
		$this->request = $this->getMockBuilder(IRequest::class)->getMock();
		$this->service = $this->getMockBuilder(FilterService::class)
			->disableOriginalConstructor()
			->getMock();
		$this->controller = new FilterController($this->request, $this->service, $this->userId);
	}

	public function testUpdate() {
		$filter = 'just check if this value is returned correctly';
		$this->service->expects($this->once())
			->method('update')
			->with($this->equalTo(3),
					$this->equalTo('title'),
					$this->equalTo('content'),
				   $this->equalTo($this->userId))
			->will($this->returnValue($filter));

		$result = $this->controller->update(3, 'title', 'content');

		$this->assertEquals($filter, $result->getData());
	}


	public function testUpdateNotFound() {
		// test the correct status code if no filter is found
		$this->service->expects($this->once())
			->method('update')
			->will($this->throwException(new FilterNotFound()));

		$result = $this->controller->update(3, 'title', 'content');

		$this->assertEquals(Http::STATUS_NOT_FOUND, $result->getStatus());
	}
}
