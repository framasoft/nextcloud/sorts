<?php

namespace OCA\Sorts\Tests\Unit\Controller;

use OCA\Sorts\Controller\FilterApiController;

class FilterApiControllerTest extends FilterControllerTest {
	public function setUp(): void {
		parent::setUp();
		$this->controller = new FilterApiController($this->request, $this->service, $this->userId);
	}
}
