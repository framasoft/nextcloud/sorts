<?php

namespace OCA\Sorts\Tests\Unit\Service;

use OCA\Sorts\Service\FilterNotFound;
use PHPUnit\Framework\TestCase;

use OCP\AppFramework\Db\DoesNotExistException;

use OCA\Sorts\Db\Filter;
use OCA\Sorts\Service\FilterService;
use OCA\Sorts\Db\FilterMapper;

class FilterServiceTest extends TestCase {
	private $service;
	private $mapper;
	private $userId = 'john';

	public function setUp(): void {
		$this->mapper = $this->getMockBuilder(FilterMapper::class)
			->disableOriginalConstructor()
			->getMock();
		$this->service = new FilterService($this->mapper);
	}

	public function testUpdate() {
		// the existing filter
		$filter = Filter::fromRow([
			'id' => 3,
			'title' => 'yo',
			'content' => 'nope'
		]);
		$this->mapper->expects($this->once())
			->method('find')
			->with($this->equalTo(3))
			->will($this->returnValue($filter));

		// the filter when updated
		$updatedFilter = Filter::fromRow(['id' => 3]);
		$updatedFilter->setTitle('title');
		$updatedFilter->setContent('content');
		$this->mapper->expects($this->once())
			->method('update')
			->with($this->equalTo($updatedFilter))
			->will($this->returnValue($updatedFilter));

		$result = $this->service->update(3, 'title', 'content', $this->userId);

		$this->assertEquals($updatedFilter, $result);
	}

	public function testUpdateNotFound() {
		$this->expectException(FilterNotFound::class);
		// test the correct status code if no filter is found
		$this->mapper->expects($this->once())
			->method('find')
			->with($this->equalTo(3))
			->will($this->throwException(new DoesNotExistException('')));

		$this->service->update(3, 'title', 'content', $this->userId);
	}
}
