# Sorts main implementation choices and their drawbacks.

This app is in Beta, and as said in, the README disclaimer, well, it's not yet
ready to be used in large Nextcloud instance. Here an explanation why, as well
as a list of planned improvements.

## Design for the filter providers

### Why `\OCP\Files\Search` and not `\OCP\Search`

"Sorts" filters are not using `\OCP\Search` but `\OCP\Files\Search`,
which mean they do not implement nor rely on other SearchProvider.

The reason is the `\OCP\Search` API is made to search for one condition
(searching for one string) on multiple set of ressources (Searching 'foo' accros
file names, mails subjects, conversation messages, ...). Each search provider
providing a new "ressource" to search into.

It is not going to work for conditional search where we want to search various
condition on various attributes of the same set of ressources (files). Some of
those attributes are not even text based (date, size, ...).

Instead, `\OCP\Files\Search` is an already existing conditional filters API,
which allow combining various condition on various metadata in one search. But
it has one major drawback : it is limited to files attribute.

### Drawbacks of `\OCP\Files\Search`

"Being limited to files attribute" mean `\OCP\Files` cannot handle searching for
tags or shares information, and even though it is all database information in the
end, `\OCP\Files\Search` is not made to be extended.

Therefore, in order to achieve combined filters on files attribute and other
things as tags or share information we need to either :

- rewrite the API (which is not gonna happen)
- Write a new system similar to this API, which incorporate search on Tags and
  Shares.
- Separate the search on files attribute, Tags and Share information and hacking
  something to combine the three.

The second solution would have been great but it also would have taken too much
time to develop, and it would have result in having yet another API to search
through files, which is not ideal.

Therefore we went for the third solution and here is how it work :

- Each time a filter is made on files attributes only, a single database request is done
  and the result could be easily paginated through
`\OCP\Files\Search` API.
- When a request is made on Tags or Share attribute only, a single request is
  done through the corresponding API and it is not paginated (at least for Tags
it seems like we can not paginate)
- When filters encompass multiples API, one request is made by API, unpaginated,
  and the result are combined with an array_intersect.

This third behaviour is a big problem because, on big instances, it can result
in three huge results arrays, which we have to intersect.

Unpaginated results can also yield to much data. Handling them can be heavy on
the server and returning them to the interface can be heavy on the web browser.

Therefore, one big improvement yet to be made to release an official version is
to implement pagination on every "type" of request (at the time of writing it is
even disable on files attribute for consistency).

Even if we can not make a single request for it, we can request paginated
result for each API which allow it, and combining them, and issue new request
until the combined results reach the pagination limit. This would mean multiple
database request for each result, but smaller one. Which could be an improvment.

## Lack of information for Shares

Actually, informations about shares come from the `\OCP\Files` API. And the only
information about shares which is yield by this API is wether or not a file or
folders have been shared with you.

This is far from ideal and we should "batch fetch" shares info from
`\OCP\Shares` every time we have some files to return.

If it can be handle in one request per batch of files as it is done with tags,
it might not be that much of an overhead.
