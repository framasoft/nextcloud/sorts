<?php

return [
	'routes' => [
		['name' => 'page#index', 'url' => '/', 'verb' => 'GET'],
		['name' => 'page#index', 'url' => '/files', 'verb' => 'GET', 'postfix' => 'files'],
		['name' => 'page#index', 'url' => '/filters', 'verb' => 'GET', 'postfix' => 'filters'],
		['name' => 'file#index', 'url' => '/nodelist', 'verb' => 'GET'],
		['name' => 'file#content', 'url' => '/nodelist/{dir}', 'verb' => 'GET', 'requirements' => ['dir' => '.+']],
		['name' => 'filter#search', 'url' => '/search', 'verb' => 'POST'],
	],
];
