<?php

namespace OCA\Sorts\AppInfo;

use OCP\AppFramework\App;

class Application extends App {
	public const APP_ID = 'sorts';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}
}
