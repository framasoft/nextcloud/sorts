# Sorts

Keep your data on sight ! "Sorts" app bring a new tree-view to your files and
folders, allowing you to "unroll" your folders, and a conditionnal filtering
engine to let you find the exact files you were looking for. 

## Disclaimer

This app is in **BETA** and release "as is". It come with no warranty
whatsoever, not even the warranty of fitness for a particular purpose (see
section 15 of the GNU agpl V3 licence in COPYING).

That said, this app shouldn't present much risk for your datas, as it does not
handle any data modification.

However, we do not recommand its usage in large Nextcloud instances yet as some
search requests are not yet paginated, and therefore can return too much results.
(more details in DESIGN.md)

## Instalation

At this time, this app is not yet publish on Nextcloud app store, but we will
be publishing it soon.

You can download the source into your ```apps/``` folder and run the command
```make```. This app will then be available on your instance and you will have
to enable it through the apps panel as an admin.

## Contribute

Contribution are welcome ! Code hosting and bug tracking are all on (Framagit)[https://framagit.org/framasoft/nextcloud/sorts].

If you want some bugs to be fix or new features to be developped you can open a
new issue in our (bug tracker 🐛)[https://framagit.org/framasoft/nextcloud/sorts/-/issues].

If you want to contribute to code, fork the code and open a Merge Request when
you are done.

If you want to help but you do not know where to start, take a look at the open
issues and DESIGN.md which explain the main choices in this app implementation
and their drawbacks.
